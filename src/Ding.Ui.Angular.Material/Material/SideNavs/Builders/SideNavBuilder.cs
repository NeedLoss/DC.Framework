﻿using Ding.Ui.Builders;

namespace Ding.Ui.Material.SideNavs.Builders {
    /// <summary>
    /// Material侧边栏导航生成器
    /// </summary>
    public class SideNavBuilder : TagBuilder {
        /// <summary>
        /// 初始化侧边栏导航生成器
        /// </summary>
        public SideNavBuilder() : base( "mat-sidenav" ) {
        }
    }
}