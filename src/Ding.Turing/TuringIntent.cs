﻿namespace Ding.Turing
{
    public class TuringIntent
    {
        public int Code { get; set; }
        public string IntentName { get; set; }

    }
}
