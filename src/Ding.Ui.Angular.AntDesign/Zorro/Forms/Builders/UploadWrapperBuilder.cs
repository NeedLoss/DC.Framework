﻿using Ding.Ui.Builders;

namespace Ding.Ui.Zorro.Forms.Builders {
    /// <summary>
    /// NgZorro文件上传包装器生成器
    /// </summary>
    public class UploadWrapperBuilder : TagBuilder {
        /// <summary>
        /// 初始化NgZorro文件上传包装器生成器
        /// </summary>
        public UploadWrapperBuilder( ) : base( "x-upload" ) {
        }
    }
}
