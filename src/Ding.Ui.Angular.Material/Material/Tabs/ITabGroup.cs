﻿using Ding.Ui.Components;
using Ding.Ui.Material.Tabs.Wrappers;
using Ding.Ui.Operations;
using Ding.Ui.Operations.Styles;

namespace Ding.Ui.Material.Tabs {
    /// <summary>
    /// 选项卡组
    /// </summary>
    public interface ITabGroup:IContainer<ITabGroupWrapper>,IColor,IBackgroundColor, IHeight {
    }
}
