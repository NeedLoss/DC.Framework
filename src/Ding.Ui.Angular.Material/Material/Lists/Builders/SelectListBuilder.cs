﻿using Ding.Ui.Builders;

namespace Ding.Ui.Material.Lists.Builders {
    /// <summary>
    /// Mat选择列表生成器
    /// </summary>
    public class SelectListBuilder : TagBuilder {
        /// <summary>
        /// 初始化选择列表生成器
        /// </summary>
        public SelectListBuilder() : base( "mat-selection-list" ) {
        }
    }
}