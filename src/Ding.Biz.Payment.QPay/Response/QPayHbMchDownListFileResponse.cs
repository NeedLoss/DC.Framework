﻿using System.Xml.Serialization;

namespace Ding.Payment.QPay.Response
{
    [XmlRoot("xml")]
    public class QPayHbMchDownListFileResponse : QPayResponse
    {
    }
}
