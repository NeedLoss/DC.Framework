﻿using Ding.Ui.Builders;

namespace Ding.Ui.Material.Tables.Builders {
    /// <summary>
    /// Material表格生成器
    /// </summary>
    public class TableBuilder : TagBuilder {
        /// <summary>
        /// 初始化表格生成器
        /// </summary>
        public TableBuilder() : base( "mat-table" ) {
        }
    }
}