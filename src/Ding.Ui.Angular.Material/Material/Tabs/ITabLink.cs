﻿using Ding.Ui.Components;
using Ding.Ui.Operations;
using Ding.Ui.Operations.Navigation;

namespace Ding.Ui.Material.Tabs {
    /// <summary>
    /// 链接选项卡
    /// </summary>
    public interface ITabLink : IComponent,ILink, ILabel, ISetIcon, IDisabled {
    }
}