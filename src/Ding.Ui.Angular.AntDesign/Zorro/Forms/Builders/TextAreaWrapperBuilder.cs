﻿using Ding.Ui.Builders;

namespace Ding.Ui.Zorro.Forms.Builders {
    /// <summary>
    ///  NgZorro多行文本框包装器生成器
    /// </summary>
    public class TextAreaWrapperBuilder : TagBuilder {
        /// <summary>
        /// 初始化NgZorro多行文本框包装器生成器
        /// </summary>
        public TextAreaWrapperBuilder() : base( "x-textarea" ) {
        }
    }
}
