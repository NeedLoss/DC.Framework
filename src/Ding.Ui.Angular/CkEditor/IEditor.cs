﻿using Ding.Ui.Components;
using Ding.Ui.Operations;
using Ding.Ui.Operations.Bind;
using Ding.Ui.Operations.Forms;

namespace Ding.Ui.CkEditor {
    /// <summary>
    /// 富文本框编辑器
    /// </summary>
    public interface IEditor : IComponent, IName, IBindName, IModel {
    }
}