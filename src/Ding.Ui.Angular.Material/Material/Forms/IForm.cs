﻿using System;
using Ding.Ui.Components;
using Ding.Ui.Operations.Events;

namespace Ding.Ui.Material.Forms {
    /// <summary>
    /// 表单
    /// </summary>
    public interface IForm : IContainer<IDisposable>, IOnSubmit {
    }
}
