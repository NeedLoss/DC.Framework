﻿using Ding.Ui.Builders;

namespace Ding.Ui.Material.Forms.Builders {
    /// <summary>
    /// Mat下拉列表包装器生成器
    /// </summary>
    class SelectWrapperBuilder : TagBuilder {
        /// <summary>
        /// 初始化Mat下拉列表包装器生成器
        /// </summary>
        public SelectWrapperBuilder( ) : base( "mat-select-wrapper" ) {
        }
    }
}
