﻿using Ding.Ui.Builders;

namespace Ding.Ui.Material.Panels.Builders {
    /// <summary>
    /// Mat面板操作生成器
    /// </summary>
    public class PanelActionBuilder : TagBuilder {
        /// <summary>
        /// 初始化面板操作生成器
        /// </summary>
        public PanelActionBuilder() : base( "mat-action-row" ) {
        }
    }
}