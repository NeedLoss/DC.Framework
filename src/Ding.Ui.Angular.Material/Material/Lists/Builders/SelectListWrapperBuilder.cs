﻿using Ding.Ui.Builders;

namespace Ding.Ui.Material.Lists.Builders {
    /// <summary>
    /// Mat选择列表包装器生成器
    /// </summary>
    public class SelectListWrapperBuilder : TagBuilder {
        /// <summary>
        /// 初始化选择列表包装器生成器
        /// </summary>
        public SelectListWrapperBuilder() : base( "mat-select-list-wrapper" ) {
        }
    }
}