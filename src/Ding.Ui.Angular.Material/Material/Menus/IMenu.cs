﻿using Ding.Ui.Components;
using Ding.Ui.Material.Menus.Wrappers;

namespace Ding.Ui.Material.Menus {
    /// <summary>
    /// 菜单
    /// </summary>
    public interface IMenu : IContainer<IMenuWrapper> {
    }
}
