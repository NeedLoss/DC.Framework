﻿using Ding.Datas.UnitOfWorks;

namespace Ding.Datas.Tests.Ef.PgSql.UnitOfWorks {
    /// <summary>
    /// PgSql工作单元
    /// </summary>
    public interface IPgSqlUnitOfWork : IUnitOfWork {
    }
}