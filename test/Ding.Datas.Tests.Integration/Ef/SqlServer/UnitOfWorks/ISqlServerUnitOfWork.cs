﻿using Ding.Datas.UnitOfWorks;

namespace Ding.Datas.Tests.Ef.SqlServer.UnitOfWorks {
    /// <summary>
    /// SqlServer工作单元
    /// </summary>
    public interface ISqlServerUnitOfWork : IUnitOfWork {
    }
}