﻿using Ding.Ui.Builders;

namespace Ding.Ui.Zorro.Tabs.Builders {
    /// <summary>
    /// NgZorro标签选项卡生成器
    /// </summary>
    public class TabBuilder : TagBuilder {
        /// <summary>
        /// 初始化标签选项卡生成器
        /// </summary>
        public TabBuilder() : base( "nz-tab" ) {
        }
    }
}