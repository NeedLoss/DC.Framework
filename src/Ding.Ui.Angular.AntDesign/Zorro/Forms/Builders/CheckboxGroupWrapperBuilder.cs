﻿using Ding.Ui.Builders;

namespace Ding.Ui.Zorro.Forms.Builders {
    /// <summary>
    /// NgZorro复选框组包装器生成器
    /// </summary>
    class CheckboxGroupWrapperBuilder : TagBuilder {
        /// <summary>
        /// 初始化NgZorro复选框组包装器生成器
        /// </summary>
        public CheckboxGroupWrapperBuilder( ) : base( "x-checkbox-group" ) {
        }
    }
}
