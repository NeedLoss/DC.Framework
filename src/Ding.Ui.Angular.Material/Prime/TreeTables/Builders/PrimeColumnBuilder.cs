﻿using Ding.Ui.Builders;

namespace Ding.Ui.Prime.TreeTables.Builders {
    /// <summary>
    /// Prime列生成器
    /// </summary>
    public class PrimeColumnBuilder : TagBuilder {
        /// <summary>
        /// 初始化列生成器
        /// </summary>
        public PrimeColumnBuilder() : base( "p-column" ) {
        }
    }
}