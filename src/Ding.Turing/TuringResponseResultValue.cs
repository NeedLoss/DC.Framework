﻿namespace Ding.Turing
{
    public class TuringResponseResultValue
    {
        public string Url { get; set; }

        public string Text { get; set; }
    }
}
