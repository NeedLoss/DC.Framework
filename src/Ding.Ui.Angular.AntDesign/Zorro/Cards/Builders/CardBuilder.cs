﻿using Ding.Ui.Builders;

namespace Ding.Ui.Zorro.Cards.Builders {
    /// <summary>
    /// NgZorro卡片生成器
    /// </summary>
    public class CardBuilder : TagBuilder {
        /// <summary>
        /// 初始化卡片生成器
        /// </summary>
        public CardBuilder() : base( "nz-card" ) {
        }
    }
}