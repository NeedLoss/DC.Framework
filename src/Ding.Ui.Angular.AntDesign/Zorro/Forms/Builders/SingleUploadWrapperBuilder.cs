﻿using Ding.Ui.Builders;

namespace Ding.Ui.Zorro.Forms.Builders {
    /// <summary>
    /// NgZorro单文件上传包装器生成器
    /// </summary>
    public class SingleUploadWrapperBuilder : TagBuilder {
        /// <summary>
        /// 初始化NgZorro单文件上传包装器生成器
        /// </summary>
        public SingleUploadWrapperBuilder( ) : base( "x-single-upload" ) {
        }
    }
}
