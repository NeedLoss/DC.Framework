﻿using Ding.Ui.Builders;

namespace Ding.Ui.Material.Dialogs.Builders {
    /// <summary>
    /// Mat弹出层操作生成器
    /// </summary>
    public class DialogActionBuilder : TagBuilder {
        /// <summary>
        /// 初始化弹出层操作生成器
        /// </summary>
        public DialogActionBuilder() : base( "mat-dialog-actions" ) {
        }
    }
}