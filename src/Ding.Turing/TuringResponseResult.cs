﻿namespace Ding.Turing
{
    public class TuringResponseResult
    {
        public int GroupType { get; set; }
        public string ResultType { get; set; }

        public TuringResponseResultValue Values { get; set; }
    }
}
