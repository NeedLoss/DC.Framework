﻿namespace Ding.Payment.Alipay.Response
{
    /// <summary>
    /// AlipayMobilePublicTemplateMessageDeleteResponse.
    /// </summary>
    public class AlipayMobilePublicTemplateMessageDeleteResponse : AlipayResponse
    {
    }
}
