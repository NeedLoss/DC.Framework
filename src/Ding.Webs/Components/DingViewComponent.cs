﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;

namespace Ding.Webs.Components
{
    /// <summary>
    /// ViewComponent的基类
    /// </summary>
    public abstract class DingViewComponent : ViewComponent
    {
        
    }
}
