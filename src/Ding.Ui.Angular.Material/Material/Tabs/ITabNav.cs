﻿using Ding.Ui.Components;
using Ding.Ui.Material.Tabs.Wrappers;
using Ding.Ui.Operations.Styles;

namespace Ding.Ui.Material.Tabs {
    /// <summary>
    /// 导航选项卡
    /// </summary>
    public interface ITabNav : IContainer<ITabNavWrapper>, IColor, IBackgroundColor {
    }
}