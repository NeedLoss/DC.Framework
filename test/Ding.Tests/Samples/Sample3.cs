﻿using System.ComponentModel;

namespace Ding.Tests.Samples {
    /// <summary>
    /// 测试样例3
    /// </summary>
    [DisplayName( "测试样例3" )]
    public class Sample3 {
        /// <summary>
        /// string值
        /// </summary>
        public string StringValue { get; set; }
    }
}
